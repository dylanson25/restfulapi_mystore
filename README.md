# RestFulApi_MyStore

The objective of this exercise is to identify the level of mastery of the tools seen during the course, which are used for the development of the BackEnd within the company HMH Systems.

The exercise consists of developing a RESTFul API with the required endpoints for the CRUD's of the resources identified in the simulated situation of a small store. The resources to work with are:

- [ ] Products
- [ ] Categories
- [ ] providers
- [ ] Purchase Orders

It is required to use the tools that have been seen during the course:

1. As Python development language in its version 3.7 or higher
2. Docker to mount the development environment
3. GitLab as project repository and change control
4. MySQL as database server
5. Postman as a tool for testing the API

## Points to evaluate:

### General features
- Compliance with the company's RESTFul standard for the design of the routes that make up the API ( URL: https://hmhsistemas.bitrix24.com/~8ch6o )
- Compliance with the naming standard for company Databases (URL: https://hmhsistemas.bitrix24.com/~BhQEG)
- Management of the Python language and its good practices
- Use of pylint for language standards compliance
- Desired API documentation with swagger 

### for GitLab
- Organization of branches (master, testing)
- Atomic and continuous commits during development
- Merge request for finished and tested resource from test to master
identifying added features and/or resolved fixes

### for Docker
- Container with the development tools used, python and server from mysql
- If more than one container is used, the communication between them as require
- docker compose definition file on GitLab

### for mysql
- ER model or UML diagram proposed for the solution
- SQL script for the creation of the database according to the model
- Folder in GitLab with the previous products
- Compliance with business rules and definition of integrity rules of the DB

### for postman
- Collection publication file and environment variables for the
API tests

## CASE OF USE
The company "Mi Tiendita" requires a system that allows the administration of the
purchase orders that you make with the different suppliers of the products that you
commercialises. The company has a catalog of N products organized by categories,
each product has only one category. The company places a weekly purchase order
by supplier, in them appear N number of different products, each product appears
only once with a price (greater than 0) and a delivery date committed in advance.
Purchase orders are sent to the representatives of each supplier to your account
of mail. Once sent to the supplier, the customer could not add or remove products to
a purchase order.
The company requests the following reports:
- List of products sold by supplier
- Price list by supplier of a particular product