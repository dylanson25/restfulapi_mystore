DROP DATABASE storeDB;
CREATE DATABASE storeDB;
use storeDB;

-- 01_add_table_category
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` TINYINT(50) AUTO_INCREMENT NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  CONSTRAINT `pk_category_id` PRIMARY KEY (`id`),
  CONSTRAINT `uq_category_name` UNIQUE (`name`)
);
-- 02_insert_category_data
LOCK TABLES `category` WRITE;
INSERT INTO `category` (`name`, `created_at`)
VALUES ('Abarrotes', "2022-06-28"),
  ('Dulceria', "2022-06-28"),
  ('Energeticos', "2022-06-28"),
  ('Vinos', "2022-06-28"),
  ('Preparados', "2022-06-28"),
  ('Agua', "2022-06-28");
UNLOCK TABLES;
-- 03_add_table_product
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` TINYINT(50) AUTO_INCREMENT NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `grams` decimal DEFAULT NULL,
  `sales_price` decimal DEFAULT NULL,
  `stock` int DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `barcode` varchar(13) DEFAULT NULL,
  `category_id` TINYINT(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  CONSTRAINT `fk_category_id` FOREIGN KEY(`category_id`) REFERENCES category(`id`),
  CONSTRAINT `pk_product_id` PRIMARY KEY (`id`),
  CONSTRAINT `ck_product_sales_price_greather_than_zero` CHECK (`sales_price` > 0)
);
-- 04_insert_product_data
LOCK TABLES `product` WRITE;
INSERT INTO `product` (
    `name`,
    `grams`,
    `sales_price`,
    `stock`,
    `category_id`,
    `created_at`,
    `description`,
    `barcode`
  )
VALUES (
    'Atun',
    120,
    12,
    20,
    1,
    "2022-06-28",
    "Lata de atun en aceite con omega 3 reforzado",
    "0024695713846"
  ),
  (
    'Frijoles',
    1000,
    18,
    7,
    1,
    "2022-06-28",
    "Frijol negro organico de zacualpan tamaulipas",
    "0024695713846"
  ),
  (
    'Caramelo de cajeta',
    3,
    4,
    25,
    2,
    "2022-06-28",
    "Dulce de leche de vaca, con colorante artificial",
    "0024695713846"
  ),
  (
    'Clorets menta',
    2,
    1,
    120,
    2,
    "2022-06-28",
    "Chicle sabor menta con 2pz",
    "0024695713846"
  ),
  (
    'Monster toronja',
    250,
    50,
    6,
    3,
    "2022-06-28",
    "Bebida energetica monster con colorante naranja sabor toronja",
    "0024695713846"
  ),
  (
    'Vive 100',
    220,
    12,
    8,
    3,
    "2022-06-28",
    "Bebida energetica sin cafe sabor a frutos rojo",
    "0024695713846"
  ),
  (
    'Centenario plata',
    450,
    299,
    3,
    4,
    "2022-06-28",
    "Bebida alcholica con 2.1% de alchol",
    "0024695713846"
  ),
  (
    'Rancho escondido',
    220,
    90,
    2,
    4,
    "2022-06-28",
    "Tequila de jalisco",
    "0024695713846"
  ),
  (
    'Sky',
    220,
    18,
    20,
    5,
    "2022-06-28",
    "Preparado de vodka con variedad de sabores",
    "0024695713846"
  ),
  (
    'Caribe Cooler',
    230,
    20,
    20,
    5,
    "2022-06-28",
    "Preparado de vodka con diferentes sabores",
    "0024695713846"
  ),
  (
    'Ciel',
    1000,
    12,
    20,
    6,
    "2022-06-28",
    "Agua purificada libre de impurezas",
    "0024695713846"
  ),
  (
    'Santorini',
    950,
    12,
    20,
    6,
    "2022-06-28",
    "Agua purificada libre de impurezas",
    "0024695713846"
  );
UNLOCK TABLES;
-- 05_add_table_provider
DROP TABLE IF EXISTS `provider`;
CREATE TABLE `provider` (
  `id` TINYINT(50) AUTO_INCREMENT NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `phone_number_agent` varchar(10) DEFAULT NULL,
  `agent` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  CONSTRAINT `pk_provider_id` PRIMARY KEY (`id`),
  CONSTRAINT `ck_provider_email_lower_Case` CHECK (`email` = lower(`email`))
);
-- 06_insert_provider_data
LOCK TABLES `provider` WRITE;
INSERT INTO `provider` (
    `email`,
    `full_name`,
    `phone_number_agent`,
    `agent`,
    `created_at`
  )
VALUES (
    "dvillarreal0@ucol.mx",
    "Dylan Ivan Villarreal Toscano",
    "3121319198",
    "Ernesto Martines Espinosa",
    "2022-06-28"
  ),
  (
    "ktovar@ucol.mx",
    "Karla Estefania Tovar",
    "3145687541",
    "Raul Lopes Amescua",
    "2022-06-28"
  ),
  (
    "juan_perez@gmail.com",
    "Juan Antonio Perez",
    "3122564719",
    "Alegandro Del poso Vaca",
    "2022-06-28"
  );
UNLOCK TABLES;
-- 08_add_table_shoping order
DROP TABLE IF EXISTS `shopping_order`;
CREATE TABLE `shopping_order` (
  `id` TINYINT(50) AUTO_INCREMENT NOT NULL,
  `confirmed` SMALLINT DEFAULT NULL,
  `date_delivery` date DEFAULT NULL,
  `total_cost` decimal DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `provider_id` TINYINT(50) null,
  CONSTRAINT `fk_provider_id` FOREIGN KEY(`provider_id`) REFERENCES provider(id),
  CONSTRAINT `pk_shopping_order_id` PRIMARY KEY (`id`)
);
-- 09_insert_shopping_order_data;
LOCK TABLES `shopping_order` WRITE;
INSERT INTO `shopping_order` (
    `confirmed`,
    `date_delivery`,
    `total_cost`,
    `created_at`,
    `provider_id`
  )
VALUES (
    1,
    "022-06-18",
    173,
    "2022-06-10",
    1
  ),
  (
    1,
    "2022-10-08",
    896,
    "2022-10-01",
    2
  ),
  (
    0,
    "2022-05-01",
    100,
    "2022-04-09",
    3
  ),
  (
    1,
    "2022-05-18",
    377.2,
    "2022-05-10",
    1
  );
UNLOCK TABLES;
-- 10_add_table_order_details
DROP TABLE IF EXISTS `product_shopping_order`;
CREATE TABLE `product_shopping_order` (
  `id` TINYINT(50) AUTO_INCREMENT NOT NULL,
  `amount` int DEFAULT NULL,
  `cost_per_unit` float DEFAULT NULL,
  `shopping_order_id` TINYINT(50) null,
  `product_id` TINYINT(50) null,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `delated_at` datetime DEFAULT NULL,
  CONSTRAINT `fk_shopping_order_id` FOREIGN KEY(`shopping_order_id`) REFERENCES shopping_order(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_id` FOREIGN KEY(`product_id`) REFERENCES product(id),
  CONSTRAINT `uq_shopping_order_id_and_product_id_unique` UNIQUE (`product_id`, `shopping_order_id`),
  CONSTRAINT `pk_product_shopping_order_id` PRIMARY KEY (`id`)
);
-- 11_insert_product_shopping_order_data
LOCK TABLES `product_shopping_order` WRITE;
INSERT INTO `product_shopping_order` (
    `amount`,
    `cost_per_unit`,
    `shopping_order_id`,
    `product_id`,
    `created_at`
  )
VALUES (10, 9.5, 1, 1, "2022-06-28"),
  (5, 15.6, 1, 2, "2022-06-28"),
  (6, 16.4, 2, 9, "2022-06-28"),
  (6, 18.7, 2, 10, "2022-06-28"),
  (4, 167.9, 2, 8, "2022-06-28"),
  (80, 0.60, 3, 4, "2022-06-28"),
  (20, 2.6, 3, 3, "2022-06-28"),
  (24, 8.6, 4, 5, "2022-06-28"),
  (4, 42.7, 4, 6, "2022-06-28");
UNLOCK TABLES;