"""Module for query response"""
import json


class Response:
    @staticmethod
    def succes_index(data):
        response = {
            'http_status_code': 200,
            'status': 'SUCCES INDEX',
            'text': 'Succes Index',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def inavalid_params(data):
        response = {
            'http_status_code': 500,
            'status': 'INVALID PARAMS',
            'text': "The paramssent are invalid",
            'data': "The paramssent are invalid"
        }
        return json.dumps(response, ensure_ascii=False), 500

    @staticmethod
    def succes_show(data):
        response = {
            'http_status_code': 200,
            'status': 'SUCCES SHOW',
            'text': 'Succes show',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def succes_created(data):
        response = {
            'http_status_code': 201,
            'status': 'SUCCES CREATION',
            'text': 'Succes Creation',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def succes_update(data):
        response = {
            'http_status_code': 200,
            'status': 'SUCCES UPDATE',
            'text': 'Succes update',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def update_response(data):
        """Response for update operation"""
        response = {
            "statusCode": 201,
            "status": "Updated",
            "text": "Success update of resource",
            "data": data
        }
        return json.dumps(response, ensure_ascii=False)

    @staticmethod
    def logical_delete(data):
        """Response for logical delete of a resource"""
        response = {
            "statusCode": 200,
            "status": "Deleted",
            "text": "Resource has been successfully deleted",
            "data": data
        }
        return json.dumps(response, ensure_ascii=False)

    @staticmethod
    def not_found(data):
        response = {
            'http_status_code': 404,
            'status': 'Resurce not found',
            'text': 'Resurce not found',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 404

    @staticmethod
    def MissingRequiredRequestParameters(data):
        response = {
            'http_status_code': 400,
            'status': 'Bad request',
            'text': f'Missing required request parameters: { data["params"] }',
            'data': data["params"]
        }
        return json.dumps(response, ensure_ascii=False), 400

    @staticmethod
    def TooWeakParams():
        response = {
            'http_status_code': 400,
            'status': 'Bad request',
            'text': 'this route can only receive one parameter',
        }
        return json.dumps(response, ensure_ascii=False), 400
    
    @staticmethod
    def NotModified():
        response = {
            'http_status_code': 304,
            'status': 'Not modified',
            'text': 'The purchase order has already been sent, therefore it cannot be modified.',
        }
        return json.dumps(response, ensure_ascii=False), 304
