flask-marshmallow==0.14.0
Flask==2.0.2
Jinja2==3.0.0
MarkupSafe==2.0.0
pymySQL==1.0.2
Werkzeug==2.0.0
Flask-SQLAlchemy==2.5.1
