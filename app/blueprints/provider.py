"""bluprint for provider"""
import json
from datetime import datetime
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from models.provider_model import Provider
from schemas.provider_schema import ProviderSchema
from schemas.products_sold_by_supplier import ProductSoldBySuplierSchema
from responses.generate_response import Response


provider_bp = Blueprint('provider', __name__, url_prefix=('/providers'))


@provider_bp.get('/')
def index():
    """List of all provider in db"""
    params = request.args
    paramKeys = list(params.keys())
    if len(paramKeys) >= 2:
        return Response.TooWeakParams()
    provider_list = Provider().get_all(params)
    if len(paramKeys) == 1 and len(params.getlist(paramKeys[0])[0]) == 0:
        return Response.MissingRequiredRequestParameters({'params': paramKeys[0]})
    if(len(provider_list) == 0):
        return Response.not_found({"param": paramKeys[0], "value":  params.getlist(paramKeys[0])[0]})

    provider = ProviderSchema(many=True).dump(provider_list)
    return Response.succes_index(provider)


@provider_bp.get('/<provider_id>')
def show(provider_id):
    """Show a product whit a selected id"""
    provider = Provider().find_by_params({'id': provider_id})
    emp = ProviderSchema(many=False).dump(provider)
    if emp:
        return Response.succes_show(emp)
    return Response.not_found(emp)


@provider_bp.get('/<provider_id>/sell-products')
def prueba_sale_price(provider_id):
    """sale price product to supliers"""
    provider_list = Provider().get_products_sold_by_supplier(provider_id)
    provider = ProductSoldBySuplierSchema(many=True).dump(provider_list)
    return Response.succes_index(provider)


@provider_bp.post('')
def create():
    """Create employe"""
    params = json.loads(request.data)
    try:
        provider = Provider(**params)
        provider.create()
    except OperationalError: 
      raise InvalidParamsException()
    except TypeError:
      raise InvalidParamsException()
    except DataError:
      raise InvalidParamsException()
    except IntegrityError:
      raise InvalidParamsException()
    created_emp = provider.find_by_params(params)
    return Response.succes_created(ProviderSchema(many=False).dump(created_emp))


@provider_bp.put('/<provider_id>')
def update(provider_id):
    """update a product"""
    body = json.loads(request.data)
    update_pro = Provider().update(provider_id, body)
    if update_pro:
        return Response.succes_update(ProviderSchema(many=False).dump(update_pro))
    return Response.not_found(update_pro)


@provider_bp.delete("/<provider_id>")
def logic_delete(provider_id):
    """Make a logical delete of an provider"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    provider = Provider().update(provider_id, {"deleted_at": deleted_at})
    if provider:
        provider = ProviderSchema(many=False).dump(provider)
        return Response.logical_delete(provider)
    return Response.not_found_response({"id": provider_id})
