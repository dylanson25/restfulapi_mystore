"""bluprint for product"""
import json
from flask import Blueprint, request
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from datetime import datetime
from models.shopping_order import ShopingOrder
from models.order_detail_model import ProductShoppingOrder
from schemas.shopping_order_schema import ShoppingOrderSchema
from schemas.products_in_shopping_order_schemas import ProductShoppingOrderSchema
from schemas.order_detail_schema import OrderDetailSchema
from responses.generate_response import Response

shopingOrder_bp = Blueprint(
    'shoping_order', __name__, url_prefix=('/shoping-orders'))


@shopingOrder_bp.get('/')
def index():
    """List of all shoping_order in db"""
    params = request.args
    paramKeys = list(params.keys())
    if len(paramKeys) >= 2:
        return Response.TooWeakParams()
    shopingOrder_list = ShopingOrder().get_all(params)
    if len(paramKeys) == 1 and len(params.getlist(paramKeys[0])[0]) == 0:
        return Response.MissingRequiredRequestParameters({'params': paramKeys[0]})
    if(len(shopingOrder_list) == 0):
        return Response.not_found({"param": paramKeys[0], "value":  params.getlist(paramKeys[0])[0]})
    shopingOrder = ShoppingOrderSchema(many=True).dump(shopingOrder_list)
    return Response.succes_index(shopingOrder)


@shopingOrder_bp.get('/<shoping_order_id>')
def show(shoping_order_id):
    """Show a shoping Order whit a selected id"""
    shopingOrder = ShopingOrder().find_by_params({'id': shoping_order_id})
    shop = ShoppingOrderSchema(many=False).dump(shopingOrder)
    if shop:
        return Response.succes_show(shop)
    return Response.not_found(shop)


@shopingOrder_bp.get('/<shopingOrder_id>/products')
def prueba_sale_price(shopingOrder_id):
    """list of products on a shoping order"""
    shopingOrder_list = ShopingOrder().get_products_in_shopping_order(shopingOrder_id)
    shopingOrder = ProductShoppingOrderSchema(
        many=True).dump(shopingOrder_list)
    return Response.succes_index(shopingOrder)


@shopingOrder_bp.post('')
def create():
    """Create shopping order"""
    params = json.loads(request.data)
    shopingOrder = ShopingOrder(**params)
    shopingOrder.create()
    created_emp = shopingOrder.find_by_params(params)
    return Response.succes_created(ShoppingOrderSchema(many=False).dump(created_emp))


@shopingOrder_bp.post('/<shopingOrder_id>/products')
def create_product(shopingOrder_id):
    """create product on a shoping order"""
    params = json.loads(request.data)
    try:
        productShopingOrder = ProductShoppingOrder(**params)
        productShopingOrder.create(shopingOrder_id)
    except OperationalError: 
      raise InvalidParamsException()
    except TypeError:
      raise InvalidParamsException()
    except DataError:
      raise InvalidParamsException()
    except IntegrityError:
      raise InvalidParamsException()
    created_pro = ProductShoppingOrder.find_last_insert()
    return Response.succes_created(OrderDetailSchema(many=True).dump(created_pro))

@shopingOrder_bp.put('/<shoppingOrder_id>')
def update(shoppingOrder_id): 
    """update a shoping order"""
    shoppingOrder =ShoppingOrderSchema(many=False).dump(ShopingOrder().find_by_params({'id': shoppingOrder_id})) 
    if shoppingOrder['confirmed'] != 0:
        return Response.NotModified()
    body = json.loads(request.data)
    update_so = ShopingOrder().update(shoppingOrder_id, body)
    if update_so:
        return Response.succes_update(ShoppingOrderSchema(many=False).dump(update_so))
    return Response.not_found(update_so)

@shopingOrder_bp.put('/<shoppingOrder_id>/products/<productShoppingOrder_id>')
def update_product(shoppingOrder_id, productShoppingOrder_id):
    """update a shoping order"""
    shoppingOrder =ShoppingOrderSchema(many=False).dump(ShopingOrder().find_by_params({'id': shoppingOrder_id})) 
    if shoppingOrder['confirmed'] != 0:
        return Response.NotModified()
    body = json.loads(request.data)
    update_so = ProductShoppingOrder().update(productShoppingOrder_id, body)
    if update_so:
        return Response.succes_update(OrderDetailSchema(many=False).dump(update_so))
    return Response.not_found(update_so)

@shopingOrder_bp.delete("/<shoppingOrder_id>")
def logic_delete(shoppingOrder_id):
    """Make a logical delete of an shopingOrder"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    shopingOrder = ShopingOrder().update(shoppingOrder_id, {"deleted_at": deleted_at})
    if shopingOrder:
        shopingOrder = ShoppingOrderSchema(many=False).dump(shopingOrder)
        return Response.logical_delete(shopingOrder)
    return Response.not_found({"id": shoppingOrder_id})

@shopingOrder_bp.delete("/<shoppingOrder_id>/products/<productShoppingOrder_id>")
def logic_delete_product(shoppingOrder_id, productShoppingOrder_id):
    """Make a logical delete of an productShoppingOrder"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    productShoppingOrder = ProductShoppingOrder().update(productShoppingOrder_id, {"delated_at": deleted_at})
    if productShoppingOrder:
        productShoppingOrder = ProductShoppingOrderSchema(many=False).dump(productShoppingOrder)
        return Response.logical_delete(productShoppingOrder)
    return Response.not_found({"id": productShoppingOrder_id})
