"""bluprint for product"""
import json
from datetime import datetime
from sqlalchemy.exc import OperationalError, DataError, IntegrityError
from errors.error_handler import InvalidParamsException
from flask import Blueprint, request
from models.product_model import Product
from models.category_model import Category
from schemas.product_schema import ProductSchema
from schemas.category_schemas import CategorySchema
from schemas.product_sale_price_schema import ProductSalePriceSchema
from responses.generate_response import Response

product_bp = Blueprint('product', __name__, url_prefix=('/products'))


@product_bp.get('/')
def index():
    """List of all product in db"""
    params = request.args
    paramKeys = list(params.keys())
    if len(paramKeys) >= 2:
        return Response.TooWeakParams()
    product_list = Product().get_all(params)
    if len(paramKeys) == 1 and len(params.getlist(paramKeys[0])[0]) == 0:
        return Response.MissingRequiredRequestParameters({'params': paramKeys[0]})
    if(len(product_list) == 0):
        return Response.not_found({"param": paramKeys[0], "value":  params.getlist(paramKeys[0])[0]})

    product = ProductSchema(many=True).dump(product_list)
    return Response.succes_index(product)


@product_bp.get('/categories')
def index_category():
    """List of all categories of products in db"""
    params = request.args
    paramKeys = list(params.keys())
    if len(paramKeys) >= 2:
        return Response.TooWeakParams()

    category_list = Category().get_all(params)

    if len(paramKeys) == 1 and len(params.getlist(paramKeys[0])[0]) == 0:
        return Response.MissingRequiredRequestParameters({'params': paramKeys[0]})

    if category_list:
        category = CategorySchema(many=True).dump(category_list)
        return Response.succes_index(category)
    return Response.not_found({"param": paramKeys[0], "value":  params.getlist(paramKeys[0])[0]})


@product_bp.get('/<product_id>')
def show(product_id):
    """Show a product whit a selected id"""
    product = Product().find_by_params({'id': product_id})
    pro = ProductSchema(many=False).dump(product)
    if pro:
        return Response.succes_show(pro)
    return Response.not_found({'id': product_id})


@product_bp.get('/<product_id>/sale-price')
def prueba_sale_price(product_id):
    """sale price product to supliers"""
    product_list = Product().get_products_sale_price(product_id)
    product = ProductSalePriceSchema(many=True).dump(product_list)
    return Response.succes_index(product)


@product_bp.post('')
def create():
    """Create product"""
    params = json.loads(request.data)
    try:
        product = Product(**params)
        product.create()
    except OperationalError: 
      raise InvalidParamsException()
    except TypeError:
      raise InvalidParamsException()
    except DataError:
      raise InvalidParamsException()
    except IntegrityError:
      raise InvalidParamsException()
    created_emp = product.find_by_params(params)
    return Response.succes_created(ProductSchema(many=False).dump(created_emp))


@product_bp.put('/<product_id>')
def update(product_id):
    """update a product"""
    body = json.loads(request.data)
    update_pro = Product().update(product_id, body)
    if update_pro:
        return Response.succes_update(ProductSchema(many=False).dump(update_pro))
    return Response.not_found(update_pro)


@product_bp.delete("/<product_id>")
def logic_delete(product_id):
    """Make a logical delete of an product"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    product = Product().update(product_id, {"deleted_at": deleted_at})
    if product:
        product = ProductSchema(many=False).dump(product)
        return Response.logical_delete(product)
    return Response.not_found_response({"id": product_id})
