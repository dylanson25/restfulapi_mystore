""" Main Flask module """
from flask import Flask
from responses.generate_response import Response
from blueprints.product import product_bp
from blueprints.provider import provider_bp
from blueprints.shopping_order import shopingOrder_bp
from config import Config
from errors.error_handler import InvalidParamsException
from db import db

application = app = Flask(__name__)
app.config.from_object(Config())
app.register_blueprint(product_bp)
app.register_blueprint(provider_bp)
app.register_blueprint(shopingOrder_bp)
db.init_app(app)


@app.errorhandler(InvalidParamsException)
def error_params(error):
    return Response.inavalid_params(None)


if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)
