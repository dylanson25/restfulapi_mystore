"""model for product_shopping_order"""
from db import db
from datetime import datetime
from errors.error_handler import InvalidParamsException


class ProductShoppingOrder(db.Model):
    """model product_shopping_order class"""

    __tablename__ = 'product_shopping_order'
    id = db.Column(db.Integer, primary_key=True, nullable=True)
    amount = db.Column(db.Integer, nullable=True)
    cost_per_unit = db.Column(db.Float, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    update_at = db.Column(db.DateTime, nullable=True)
    delated_at = db.Column(db.DateTime, nullable=True)
    product_id = db.Column(db.Integer, db.ForeignKey(
        "product.id"), nullable=False)
    shopping_order_id = db.Column(db.Integer, db.ForeignKey(
        "shopping_order.id"), nullable=False)
    fields = [
        'id',
        'amount',
        'cost_per_unit',
        'shopping_order_id',
        'product_id',
        'created_at',
        'update_at',
        'delated_at'
    ]

    def __validate_params(self, params):
        """validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def create(self, shoppingOrder_id):
        """Create an product"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.shopping_order_id = shoppingOrder_id
        db.session.add(self)
        print(self.id)
        db.session.commit()
    
    def find_last_insert():
        query = f"""
            SELECT * 
            FROM product_shopping_order
            ORDER BY id DESC
            LIMIT 1;
        """
        with db.engine.connect() as con:
            result = con.exec_driver_sql(query)
            return result

    def update(self, productShoppingOrder_id, params):
        """Update provide in db"""
        productShoppingOrder = self.find_by_params({"id": productShoppingOrder_id, "delated_at": None})
        if productShoppingOrder:
            for key, value in params.items():
                setattr(productShoppingOrder, key, value)
            db.session.commit()
            return self.find_by_params({"id": productShoppingOrder_id})
        return None

    def find_by_params(self, params):
        """Get the first  coinciding"""
        return self.query.filter_by(**params).first()

    def get_all(self, params=None):
        """get all"""
        self.__validate_params(params)
        return self.query.filter_by(deleted_at=None, **params).all()
