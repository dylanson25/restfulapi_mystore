"""model for Shoping Order"""
from db import db
from datetime import datetime
from errors.error_handler import InvalidParamsException
from models.order_detail_model import ProductShoppingOrder


class ShopingOrder(db.Model):
    """model Shoping Order class"""

    __tablename__ = 'shopping_order'
    id = db.Column(db.Integer, primary_key=True)
    confirmed = db.Column(db.SmallInteger, nullable=True)
    date_delivery = db.Column(db.DateTime, nullable=True)
    total_cost = db.Column(db.Float, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    update_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    provider_id = db.Column(db.Integer, db.ForeignKey(
        "provider.id"), nullable=False)
    productShoppingOrder = db.relationship('ProductShoppingOrder')
    fields = [
        'id',
        'confirmed',
        'date_delivery',
        'total_cost',
        'provider_id',
        'update_at',
        'deleted_at',
        'created_at'
    ]

    def __validate_params(self, params):
        """validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """get all"""
        self.__validate_params(params)
        return self.query.filter_by(deleted_at=None, **params).all()

    def find_by_params(self, params):
        """Get the first  coinciding"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create an product"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
    
    def update(self, shopingOrder_id, params):
        """Update provide in db"""
        shopingOrder = self.find_by_params({"id": shopingOrder_id, "deleted_at": None})
        if shopingOrder:
            for key, value in params.items():
                setattr(shopingOrder, key, value)
            db.session.commit()
            return self.find_by_params({"id": shopingOrder_id})
        return None

    def get_products_in_shopping_order(self, shopping_order_id):
        query = f"""
        select so.id as 'shopping_order_id',pro.full_name as 'provider',  p.name, pso.cost_per_unit, pso.amount, (pso.amount * pso.cost_per_unit) as 'subtotal' from product p
	        inner join product_shopping_order pso
	            on pso.product_id = p.id
            inner join  shopping_order so
                on so.id = pso.shopping_order_id
            inner join provider pro
                on pro.id = so.provider_id
	        where so.id = {shopping_order_id}
            order by pro.full_name;
        """
        with db.engine.connect() as con:
            result = con.exec_driver_sql(query)
            return result
