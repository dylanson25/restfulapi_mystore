"""model for category"""
from db import db
from errors.error_handler import InvalidParamsException
from models.product_model import Product


class Category(db.Model):
    """model category class"""

    __tablename__ = 'category'
    id = db.Column(db.Integer,  primary_key=True)
    name = db.Column(db.Text(45), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    product = db.relationship('Product')
    fields = [
        'id',
        'name',
        'created_at',
        'deleted_at',
        'updated_at'
    ]

    def __validate_params(self, params):
        """validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """get all"""
        self.__validate_params(params)
        return self.query.filter_by(deleted_at=None, **params).all()

   
