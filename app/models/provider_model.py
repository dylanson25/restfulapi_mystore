"""model for provider"""
from db import db
from datetime import datetime
from errors.error_handler import InvalidParamsException
from models.shopping_order import ShopingOrder


class Provider(db.Model):
    """model provider class"""

    __tablename__ = 'provider'
    id = db.Column(db.Integer,  primary_key=True)
    email = db.Column(db.Text(50), nullable=True)
    full_name = db.Column(db.Text(50), nullable=True)
    phone_number_agent = db.Column(db.Text(10), nullable=True)
    agent = db.Column(db.Text(40), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    shopingOrder = db.relationship('ShopingOrder')
    fields = [
        'id',
        'email',
        'full_name',
        'phone_number_agent',
        'agent',
        'created_at',
        'deleted_at',
        'updated_at',
    ]

    def __validate_params(self, params):
        """validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """get all"""
        self.__validate_params(params)
        return self.query.filter_by(deleted_at=None, **params).all()

    def find_by_params(self, params):
        """Get the first  coinciding"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create an provider"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()

    def update(self, provider_id, params):
        """Update provide in db"""
        provider = self.find_by_params({"id": provider_id, "deleted_at": None})
        if provider:
            for key, value in params.items():
                setattr(provider, key, value)
            db.session.commit()
            return self.find_by_params({"id": provider_id})
        return None
    
    def get_products_sold_by_supplier(self, provider_id):
        query = f"""
            select pro.full_name, so.id as 'shopping_order_id', p.name, pso.cost_per_unit from product p
	        inner join product_shopping_order pso
	            on pso.product_id = p.id
            inner join  shopping_order so
                on so.id = pso.shopping_order_id
            inner join provider pro
                on pro.id = so.provider_id
	        where so.provider_id = {provider_id}
            order by pro.full_name;
        """
        with db.engine.connect() as con:
            result = con.exec_driver_sql(query)
            return result
