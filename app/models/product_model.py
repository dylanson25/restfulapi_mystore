"""product model"""
from db import db
from datetime import datetime
from errors.error_handler import InvalidParamsException
from models.order_detail_model import ProductShoppingOrder


class Product(db.Model):
    """model product class"""

    __tablename__ = 'product'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=True)
    grams = db.Column(db.Float, nullable=True)
    sales_price = db.Column(db.Float, nullable=True)
    stock = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String(200), nullable=True)
    barcode = db.Column(db.String(13), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey(
        'category.id'), nullable=False)
    productShoppingOrder = db.relationship('ProductShoppingOrder')
    fields = [
        'id',
        'name',
        'grams',
        'sales_price',
        'stock',
        'description',
        'barcode',
        'category_id',
        'created_at',
        'deleted_at',
        'updated_at'
    ]

    def __validate_params(self, params):
        """validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()

    def get_all(self, params=None):
        """get all"""
        self.__validate_params(params)
        return self.query.filter_by(deleted_at=None, **params).all()

    def create(self):
        """Create an product"""
        self.created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()

    def find_by_params(self, params):
        """Get the first  coinciding"""
        return self.query.filter_by(**params).first()

    def update(self, product_id, params):
        """Update product in db"""
        product = self.find_by_params({"id": product_id, "deleted_at": None})
        if product:
            for key, value in params.items():
                setattr(product, key, value)
            db.session.commit()
            return self.find_by_params({"id": product_id})
        return None

    def get_one_by(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def example_product(self, params):
        """logic delete an employee status by the given id"""
        product = self.get_one_by(params)
        if product:
            product.deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            db.session.commit()
            return product
        return None

    def get_products_sale_price(self, product_id):
        """get a sale price to provider of a selected product"""
        query = f"""
            select pro.full_name, p.id, p.name, pso.cost_per_unit from product as p
	        inner join product_shopping_order as pso
	            on pso.product_id = p.id
            inner join  shopping_order as so
                on so.id = pso.shopping_order_id
            inner join provider as pro
                on pro.id = so.provider_id
	        where p.id = {product_id}
            order by pro.full_name;
        """
        with db.engine.connect() as con:
            result = con.exec_driver_sql(query)
            return result
