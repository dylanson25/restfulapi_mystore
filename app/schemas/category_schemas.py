"""schema for category"""
from ma import ma


class CategorySchema(ma.Schema):
    """Category schema class"""
    class Meta:
        fields = (
            'id',
            'name'
        )
