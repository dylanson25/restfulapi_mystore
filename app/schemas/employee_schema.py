"""Schema for Employee"""
from ma import ma


class EmployeeSchema(ma.Schema):
    """Employee schema class"""
    class Meta:
        fields = ('id',
                  'name_full',
                  'rfc',
                  'is_active',
                  'salary')
