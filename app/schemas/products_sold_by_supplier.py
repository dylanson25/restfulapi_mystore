"""Schema for products sold by suplier"""
from ma import ma


class ProductSoldBySuplierSchema(ma.Schema):
    """Product_sold_by_suplier schema class"""
    class Meta:
        fields = (
            'full_name',
            'shopping_order_id',
            'name',
            'cost_per_unit',
        )
