"""Schema for order_detail"""
from ma import ma 

class OrderDetailSchema(ma.Schema):
    """Order detail schema class"""
    class Meta:
        fields = (
            'id',
            'amount',
            'cost_per_unit',
            'shopping_order_id',
            'product_id'
        )