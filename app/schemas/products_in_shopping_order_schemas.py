"""Schema for Employee"""
from ma import ma


class ProductShoppingOrderSchema(ma.Schema):
    """Employee schema class"""
    class Meta:
        fields = ('shopping_id',
                  'provider',
                  'name',
                  'cost_per_unit',
                  'amount',
                  'subtotal'
                  )
