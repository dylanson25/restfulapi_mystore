"""Schema for provider"""
from ma import ma 

class ProviderSchema(ma.Schema):
    """Product schema class"""
    class Meta:
        fields = (
            'id',
            'email',
            'full_name',
            'phone_number_agent',
            'agent'
        )