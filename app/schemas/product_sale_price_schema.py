"""Schema for Product sale price"""
from ma import ma


class ProductSalePriceSchema(ma.Schema):
    """Product sale price schema class"""
    class Meta:
        fields = (
            'full_name',
            'id',
            'name',
            'cost_per_unit'
        )
