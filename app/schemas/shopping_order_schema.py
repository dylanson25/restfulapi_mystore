"""Schema for shopping order"""
from ma import ma 

class ShoppingOrderSchema(ma.Schema):
    """Shopping order schema class"""
    class Meta:
        fields = (
            'id',
            'confirmed',
            'date_delivery',
            'total_cost',
            'provider_id'
        )