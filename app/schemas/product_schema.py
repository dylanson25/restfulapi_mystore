"""Schema for product"""
from ma import ma


class ProductSchema(ma.Schema):
    """Product schema class"""
    class Meta:
        fields = (
            'id',
            'name',
            'grams',
            'sales_price',
            'stock',
            'description',
            'barcode',
            'category_id'
        )
